<?php

class SearchApiEntityClaimDataSourceController extends SearchApiEntityDataSourceController {

  protected $table = 'search_api_item_claim';

  protected $lease_time = 30;

  public function getChangedItems(SearchApiIndex $index, $limit = -1) {
    if ($limit == 0) {
      return array();
    }
    $this->checkIndex($index);
    $select = db_select($this->table, 't');
    $select->addField('t', 'item_id');
    $select->condition($this->indexIdColumn, $index->id);
    $select->condition($this->changedColumn, 0, '>');
    $select->orderBy($this->changedColumn, 'ASC');
    $select->condition('claimed', 0);
    if ($limit > 0) {
      $select->range(0, $limit);
    }

    $ids = $select->execute()->fetchCol();

    if (empty($ids)) {
      // See if there are stale items, still to index.
      $select = db_select($this->table, 't');
      $select->addField('t', 'item_id');
      $select->condition($this->indexIdColumn, $index->id);
      $select->condition($this->changedColumn, 0, '>');
      $select->orderBy($this->changedColumn, 'ASC');
      $select->condition('claimed', time(), '<');
      if ($limit > 0) {
        $select->range(0, $limit);
      }

      $ids = $select->execute()->fetchCol();
    }

    if (!empty($ids)) {

      $update = db_update($this->table)
        ->fields(array(
          'claimed' => time() + $this->lease_time,
        ))
        ->condition('item_id', $ids, 'IN');

      $update->execute();
    }

    return $ids;


  }

  public function trackItemIndexed(array $item_ids, SearchApiIndex $index) {
    if (!$this->table) {
      return;
    }
    $this->checkIndex($index);
    db_update($this->table)
      ->fields(array(
        $this->changedColumn => 0,
        'claimed' => 0,
      ))
      ->condition($this->itemIdColumn, $item_ids, 'IN')
      ->condition($this->indexIdColumn, $index->id)
      ->execute();
  }


}